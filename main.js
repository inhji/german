import './style.css'
import data from './verbs.js'

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

const all = data.verbs.reduce((arr, verb) => {
  return arr.concat(data.prefixes.map(prefix => {
    return prefix + verb
  }))
}, [])

const randomIndex = getRandomInt(0, all.length - 1)
const randomWord = all[randomIndex]

document.querySelector('#app').innerHTML = `
  
  <h1>What does '<a href='https://dict.cc/${randomWord}'>${randomWord}</a>' mean?</h1>
  <h2>And does this word even exist? 🤔</h2>

  <p>This word comes from a collection of ${data.verbs.length*data.prefixes.length} verbs made from only ${data.verbs.length} verbs and ${data.prefixes.length} prefixes. See how lazy germans are?</p>
`
