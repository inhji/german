export default {
	verbs: [
		"drücken",
		"fahren",
		"fallen",
		"gehen",
		"lassen",
		"laufen",		
		"legen",
		"halten",
		"machen",
		"sagen",
		"schlagen",
		"sehen",
		"stehen",
		"setzen",
		"steigen",
		"treten",
		"werfen"
	],
	prefixes: [
		"ab",
		"an",
		"auf",
		"aus",
		"ent",
		"fort",
		"nach",
		"um",
		"ver",
		"vor",
		"weg",
		"zurück"
	]
}