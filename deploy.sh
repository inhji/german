#!/usr/bin/env bash

ZANN_HOST=inhji.de
ZANN_DIR=/home/inhji/www/german

echo ""
echo "Pushing changes..."
echo "--------------------------"
#rsync -avz dist $ZANN_HOST:$ZANN_DIR --progress

rsync -chavzP --stats ./dist $ZANN_HOST:$ZANN_DIR 
